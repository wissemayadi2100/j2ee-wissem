FROM tomcat:latest

LABEL maintainer="wissem"

COPY  ./webapp/target/webapp.war /usr/local/tomcat/webapps/

CMD ["catalina.sh", "run"] 




